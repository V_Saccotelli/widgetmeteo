var doc = document;
let idCitta;

doc.addEventListener('DOMContentLoaded', function () {
    doc.querySelector('#btnCittaValue').addEventListener('click', leggiCitta);
    function leggiCitta() {
        citta = doc.getElementById('valoreCitta').value;
        let meteo = 'https://api.openweathermap.org/data/2.5/weather?q=' + citta + '&appid=57af13e0259faad1e5fa958c1aac9e09'
        fetch(meteo).then(response => response.json()).then(json => {
            let obj = json;
            console.log(json);
            doc.querySelector('#nomeCitta').innerHTML = obj.name;
            doc.querySelector('#dettaglioTempo').innerHTML = obj.weather[0].main;
            doc.querySelector('#temp').innerHTML = Math.floor(obj.main.temp - 273.15) + ' ' + '°C';
            doc.querySelector('#icon').src = "http://openweathermap.org/img/w/" + obj.weather[0].icon + ".png";
            doc.querySelector('#umidita').innerHTML = 'Umidità: ' + ' ' + obj.main.humidity + '%';
            idCitta = obj.id;
            console.log(idCitta);

            /* Seconda chiamata fetch (meteo 5 giorni) */

            let meteoCinqueGiorni = 'https://api.openweathermap.org/data/2.5/forecast?id=' + idCitta + '&appid=57af13e0259faad1e5fa958c1aac9e09'
            fetch(meteoCinqueGiorni).then(response => response.json()).then(json => {
                let obj2 = json;
                console.log(obj2);

                obj2.list.forEach(item => {
                    console.log(item)
                    doc.querySelector('#meteoOra').innerHTML += ` <tr>
                    <td><img src="http://openweathermap.org/img/w/${item.weather[0].icon}.png"></td>
                    <td>${item.dt_txt}</td>
                    <td>${Math.floor(item.main.temp_min - 273.15) + ' ' + '°C'}</td>
                    <td>${Math.floor(item.main.temp - 273.15) + ' ' + '°C'}</td>
                    <td>${item.main.humidity}%</td>
                    </tr>`
                });
            })
        })
    }
})

